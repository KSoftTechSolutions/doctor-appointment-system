<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

if ( ! isset($config))
{
    $config = array();
}


$config = array_merge($config, array(
    'department_listing_headers'  => array(
        'checkbox'   => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'dept_name'  => array(
            'jsonField' => 'dept_name',
            'width'     => '20%'
        ),
        'created_at' => array(
            'jsonField' => 'created_at',
            'width'     => '10%'
        ),
        'updated_at' => array(
            'jsonField' => 'updated_at',
            'width'     => '10%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON'   => 'id',
                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON'   => 'admin/department/edit/',
                'DELETE_ICON' => 'admin/department/delete/'
            ),
        )
    ),
    'doctor_listing_headers'      => array(
        'checkbox'   => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'dept_name'  => array(
            'jsonField' => 'dept_name',
            'width'     => '20%'
        ),
        'name'       => array(
            'jsonField' => 'name',
            'width'     => '20%'
        ),
        'email'      => array(
            'jsonField' => 'email',
            'width'     => '10%'
        ),
        'mobile'     => array(
            'jsonField' => 'mobile',
            'width'     => '10%'
        ),
        'address'    => array(
            'jsonField' => 'address',
            'width'     => '20%'
        ),
        'created_at' => array(
            'jsonField' => 'created_at',
            'width'     => '10%'
        ),
        'updated_at' => array(
            'jsonField' => 'updated_at',
            'width'     => '10%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON'   => 'id',
                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON'   => 'admin/doctor/edit/',
                'DELETE_ICON' => 'admin/doctor/delete/'
            ),
        )
    ),
    'patient_listing_headers'     => array(
        'checkbox'   => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'name'       => array(
            'jsonField' => 'name',
            'width'     => '10%'
        ),
        'dob'        => array(
            'jsonField' => 'dob',
            'width'     => '5%'
        ),
        'gender'     => array(
            'jsonField' => 'gender',
            'width'     => '5%'
        ),
        'email'      => array(
            'jsonField' => 'email',
            'width'     => '10%'
        ),
        'mobile'     => array(
            'jsonField' => 'mobile',
            'width'     => '5%'
        ),
        'address'    => array(
            'jsonField' => 'address',
            'width'     => '10%'
        ),
        //        'height' => array(
        //            'jsonField' => 'height',
        //            'width'     => '5%'
        //        ),
        //        'weight' => array(
        //            'jsonField' => 'weight',
        //            'width'     => '5%'
        //        ),
        //        'blood_group' => array(
        //            'jsonField' => 'blood_group',
        //            'width'     => '5%'
        //        ),
        //        'blood_pressure' => array(
        //            'jsonField' => 'blood_pressure',
        //            'width'     => '5%'
        //        ),
        //        'past_disease' => array(
        //            'jsonField' => 'past_disease',
        //            'width'     => '5%'
        //        ),
        //        'allergies' => array(
        //            'jsonField' => 'allergies',
        //            'width'     => '5%'
        //        ),
        'created_at' => array(
            'jsonField' => 'created_at',
            'width'     => '10%'
        ),
        'updated_at' => array(
            'jsonField' => 'updated_at',
            'width'     => '10%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'VIEW_ICON',
                'EDIT_ICON',
                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'VIEW_ICON'   => 'id',
                'EDIT_ICON'   => 'id',
                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'VIEW_ICON'   => 'admin/patient/view/',
                'EDIT_ICON'   => 'admin/patient/edit/',
                'DELETE_ICON' => 'admin/patient/delete/'
            ),
        )
    ),
    'appointment_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'appnt_no'     => array(
            'jsonField' => 'appnt_no',
            'width'     => '20%'
        ),
        'patient_name' => array(
            'jsonField' => 'patient_name',
            'width'     => '10%'
        ),
        'booking_date' => array(
            'jsonField' => 'booking_date',
            'width'     => '10%'
        ),
        'booking_time' => array(
            'jsonField' => 'booking_time',
            'width'     => '10%'
        ),
        'created_at'   => array(
            'jsonField' => 'created_at',
            'width'     => '10%'
        ),
        'updated_at'   => array(
            'jsonField' => 'updated_at',
            'width'     => '10%'
        ),
        'action'       => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '10%',
            'align'          => 'center',
            'type'           => array(
                'VIEW_ICON',
                'EDIT_ICON',
                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'VIEW_ICON'   => 'id',
                'EDIT_ICON'   => 'id',
                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'VIEW_ICON'   => 'admin/appointment/view/',
                'EDIT_ICON'   => 'admin/appointment/edit/',
                'DELETE_ICON' => 'admin/appointment/delete/'
            ),
        )
    ),
));