<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}


$config['css_default'] = array(
    'bootstrap.css'             => array('name' => 'assets/admin/app-assets/css/bootstrap.css'),
    'icomoon.css'               => array('name' => 'assets/admin/app-assets/fonts/icomoon.css'),
    'flag-icon.min.css'         => array('name' => 'assets/admin/app-assets/fonts/flag-icon-css/css/flag-icon.min.css'),
    'font-awesome.min.css'      => array('name' => 'assets/css/font-awesome/css/font-awesome.min.css'),
    'pace.css'                  => array('name' => 'assets/admin/app-assets/vendors/css/extensions/pace.css'),
    'bootstrap-extended.css'    => array('name' => 'assets/admin/app-assets/css/bootstrap-extended.css'),
    'app.css'                   => array('name' => 'assets/admin/app-assets/css/app.css'),
    'colors.css'                => array('name' => 'assets/admin/app-assets/css/colors.css'),
    'vertical-menu.css'         => array('name' => 'assets/admin/app-assets/css/core/menu/menu-types/vertical-menu.css'),
    'vertical-overlay-menu.css' => array('name' => 'assets/admin/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css'),
    'palette-gradient.css'      => array('name' => 'assets/admin/app-assets/css/core/colors/palette-gradient.css'),
    'style.css'                 => array('name' => 'assets/admin/assets/css/style.css'),

    'custom.css' => array('name' => 'assets/css/module/admin/custom.css'),
);

$config['js_default'] = array(
    'jquery.min.js'        => array('name' => 'assets/admin/app-assets/js/core/libraries/jquery.min.js'),
    'tether.min.js'        => array('name' => 'assets/admin/app-assets/vendors/js/ui/tether.min.js'),
    'bootstrap.min.js'     => array('name' => 'assets/admin/app-assets/js/core/libraries/bootstrap.min.js'),
    'perfect-scrollbar.js' => array('name' => 'assets/admin/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js'),
    'unison.min.js'        => array('name' => 'assets/admin/app-assets/vendors/js/ui/unison.min.js'),
    'blockUI.min.js'       => array('name' => 'assets/admin/app-assets/vendors/js/ui/blockUI.min.js'),
    'screenfull.min.js'    => array('name' => 'assets/admin/app-assets/vendors/js/ui/screenfull.min.js'),
    'pace.min.js'          => array('name' => 'assets/admin/app-assets/vendors/js/extensions/pace.min.js'),
    'app-menu.js'          => array('name' => 'assets/admin/app-assets/js/core/app-menu.js'),
    'app.js'               => array('name' => 'assets/admin/app-assets/js/core/app.js'),

    'custom.js' => array('name' => 'assets/js/module/admin/custom.js'),

    'jquery.validate.js'             => array('name' => 'assets/js/plugin/jquery-validate/jquery.validate.js'),
    'validate-additional-methods.js' => array('name' => 'assets/js/plugin/jquery-validate/additional-methods.js'),
    'alphanumeric.js'                => array('name' => 'assets/js/plugin/alphanumeric/alphanum.js'),
);

$config['css_arr'] = array(
    'dataTables.bootstrap4' => array('name' => 'assets/admin/assets/js/data-tables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css'),
    'buttons.dataTables'    => array('name' => 'assets/admin/assets/js/data-tables/Buttons-1.5.1/css/buttons.dataTables.min.css'),
    'select.bootstrap4'     => array('name' => 'assets/admin/assets/js/data-tables/Select-1.2.5/css/select.bootstrap4.min.css'),

    'bootstrap-datepicker' => array('name' => 'assets/js/plugin/boostrap-datepicker/css/bootstrap-datepicker.min.css'),
);

$config['js_arr'] = array(
    'jquery.dataTables'     => array('name' => 'assets/admin/assets/js/data-tables/DataTables-1.10.16/js/jquery.dataTables.min.js'),
    'dataTables.bootstrap4' => array('name' => 'assets/admin/assets/js/data-tables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js'),
    'dataTables.buttons'    => array('name' => 'assets/admin/assets/js/data-tables/Buttons-1.5.1/js/dataTables.buttons.min.js'),
    'jszip'                 => array('name' => 'assets/admin/assets/js/data-tables/JSZip-2.5.0/jszip.min.js'),
    'pdfmake'               => array('name' => 'assets/admin/assets/js/data-tables/pdfmake-0.1.32/pdfmake.min.js'),
    'vfs_fonts'             => array('name' => 'assets/admin/assets/js/data-tables/pdfmake-0.1.32/vfs_fonts.js'),
    'buttons.html5'         => array('name' => 'assets/admin/assets/js/data-tables/Buttons-1.5.1/js/buttons.html5.min.js'),
    'buttons.print'         => array('name' => 'assets/admin/assets/js/data-tables/Buttons-1.5.1/js/buttons.print.min.js'),
    'dataTables.select'     => array('name' => 'assets/admin/assets/js/data-tables/Select-1.2.5/js/dataTables.select.min.js'),
    'dataTables-main'       => array('name' => 'assets/admin/assets/js/data-tables/dataTables-main.js'),


    'bootstrap-datepicker' => array('name' => 'assets/js/plugin/boostrap-datepicker/js/bootstrap-datepicker.min.js'),
);
