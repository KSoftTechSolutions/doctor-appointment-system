<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']   = 'public/index/index';
$route['404_override']         = '';
$route['translate_uri_dashes'] = FALSE;

/**
 * Auth
 */
$route['admin']                 = 'admin/index';
$route['admin/login']           = 'auth/index/login';
$route['admin/forgot-password'] = 'auth/index/forgot_password';
$route['admin/reset-password']  = 'auth/index/reset_password';
$route['admin/logout']          = 'auth/index/logout';

/**
 * Admin Routes
 */
$route['admin/dashboard'] = 'admin/index/index';

//department routes
$route['admin/department/add']           = 'admin/department/add';
$route['admin/department/list']          = 'admin/department/index';
$route['admin/department/edit/(:num)']   = 'admin/department/add/$1';
$route['admin/department/delete/(:num)'] = 'admin/department/delete/$1';

//doctor routes
$route['admin/doctor/add']           = 'admin/doctor/add';
$route['admin/doctor/list']          = 'admin/doctor/index';
$route['admin/doctor/edit/(:num)']   = 'admin/doctor/add/$1';
$route['admin/doctor/delete/(:num)'] = 'admin/doctor/delete/$1';

//patient routes
$route['admin/patient/add']           = 'admin/patient/add';
$route['admin/patient/list']          = 'admin/patient/index';
$route['admin/patient/edit/(:num)']   = 'admin/patient/add/$1';
$route['admin/patient/delete/(:num)'] = 'admin/patient/delete/$1';

//appointment routes
$route['admin/appointment/add']           = 'admin/appointment/add';
$route['admin/appointment/list']          = 'admin/appointment/index';
$route['admin/appointment/view/(:num)']   = 'admin/appointment/appointment_view/$1';
$route['admin/appointment/edit/(:num)']   = 'admin/appointment/add/$1';
$route['admin/appointment/delete/(:num)'] = 'admin/appointment/delete/$1';

/**
 * Public Routes
 */
$route['home']                      = 'public/index/home';
$route['about-us']                  = 'public/index/about_us';
$route['contact-us']                = 'public/index/contact_us';
$route['department']                = 'public/index/department';
$route['appointment']               = 'public/appointment/index';
$route['appointment-detail/(:num)'] = 'public/appointment/appointment_detail/$1';
$route['under-construction']        = 'public/index/under_construction';

$route['patient/login']        = 'public/patient/login';
$route['patient/logout']       = 'public/patient/logout';
$route['patient/registration'] = 'public/patient/register';

$route['appointment/get-doctor-by-dept-ajax'] = 'public/appointment/get_doctor_by_department';

