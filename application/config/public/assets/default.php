<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}


$config['css_default'] = array(
    'bootstrap.css'         => array('name' => 'assets/public/css/bootstrap.css'),
    'style.css'             => array('name' => 'assets/public/css/style.css'),
    'appointment_style.css' => array('name' => 'assets/public/css/appointment_style.css'),
    'font-awesome.css'      => array('name' => 'assets/public/css/font-awesome.css'),

    'custom.css' => array('name' => 'assets/css/module/public/custom.css'),
);
$config['js_default']  = array(
    'jquery.min.js' => array('name' => 'assets/public/js/jquery-2.1.4.min.js'),
    'bootstrap.js'  => array('name' => 'assets/public/js/bootstrap.js'),

    'custom.js' => array('name' => 'assets/js/module/public/custom.js'),
);

$config['css_arr'] = array(
    'bootstrap-datepicker' => array('name' => 'assets/js/plugin/boostrap-datepicker/css/bootstrap-datepicker.min.css'),
);
$config['js_arr']  = array(
    'jquery.validate'             => array('name' => 'assets/js/plugin/jquery-validate/jquery.validate.js'),
    'validate-additional-methods' => array('name' => 'assets/js/plugin/jquery-validate/additional-methods.js'),
    'alphanumeric'                => array('name' => 'assets/js/plugin/alphanumeric/alphanum.js'),
    'bootstrap-datepicker'        => array('name' => 'assets/js/plugin/boostrap-datepicker/js/bootstrap-datepicker.min.js'),

    //pages
    'appointment'                 => array('name' => 'assets/js/module/public/appointment.js'),
    'patient'                     => array('name' => 'assets/js/module/public/patient.js'),
);