<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

if ( ! isset($config))
{
    $config = array();
}


$config = array_merge($config, array(
    'session_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'ses_start_yr' => array(
            'jsonField' => 'ses_start_yr',
            'width'     => '25%'
        ),
        'ses_end_yr'   => array(
            'jsonField' => 'ses_end_yr',
            'width'     => '20%'
        ),
        'created_at'   => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'   => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'       => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON'   => 'id',
                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON'   => 'admin/session/edit/',
                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));