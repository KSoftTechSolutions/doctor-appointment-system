<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function get_admin_primary_menu()
{
    $CI       = &get_instance();
    $top_menu = $CI->config->item('admin_top_menu');

    $menu = '<ul class="nav navbar-nav">';
    foreach ($top_menu as $key => $value)
    {
        if (is_array($value))
        {
            $menu .= '<li class="dropdown">'
                     . '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' . $key . '<span class="caret"></span></a>'
                     . '<ul class="dropdown-menu" role="menu">';
            foreach ($value as $sub_key => $sub_val)
            {
                if (is_array($sub_val))
                {
                    $menu .= '<li class="dropdown has-sub-sub-menu-li">'
                             . '<a href="#" class="dropdown-toggle has-sub-sub-menu" data-toggle="dropdown">' . $sub_key . '<span class="caret"></span></a>'
                             . '<ul class="dropdown-menu sub-sub-menu" role="menu">';
                    foreach ($sub_val as $sub_sub_key => $sub_sub_val)
                    {
                        $menu .= '<li><a href="' . base_url($sub_sub_val) . '">' . $sub_sub_key . '</a></li>';
                    }
                    $menu .= '</ul>';
                }
                else
                {
                    $menu .= '<li><a href="' . base_url($sub_val) . '">' . $sub_key . '</a></li>';
                }
            }
            $menu .= '</ul>';
        }
        else
        {
            $menu .= '<li><a href="' . base_url($value) . '">' . $key . '</a></li>';
        }
    }
    $menu .= '</ul>';

    return $menu;
    ?>
    <?php

}

function password_decrypt($password, $hash)
{
    return crypt($password, $hash);
}

function password_encrypt($password)
{
    $cost = 10;
    $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
    $salt = sprintf("$2a$%02d$", $cost) . $salt;
    $hash = crypt($password, $salt);

    return $hash;
}

function get_appointment_slots()
{
    $slots = [
        '10:00 AM' => '10:00 AM',
        '10:15 AM' => '10:15 AM',
        '10:30 AM' => '10:30 AM',
        '10:45 AM' => '10:45 AM',
        '11:00 AM' => '11:00 AM',
        '11:15 AM' => '11:15 AM',
        '11:30 AM' => '11:30 AM',
        '11:45 AM' => '11:45 AM',
        '12:00 PM' => '12:00 PM',
        '12:15 PM' => '12:15 PM',
        '12:30 PM' => '12:30 PM',
        '12:45 PM' => '12:45 PM',
        '01:00 PM' => '01:00 PM',
        '01:15 PM' => '01:15 PM',
        '01:30 PM' => '01:30 PM',
        '01:45 PM' => '01:45 PM',
        '02:00 PM' => '02:00 PM',
        '02:15 PM' => '02:15 PM',
        '02:30 PM' => '02:30 PM',
        '02:45 PM' => '02:45 PM',
        '03:00 PM' => '03:00 PM',
        '03:15 PM' => '03:15 PM',
        '03:30 PM' => '03:30 PM',
        '03:45 PM' => '03:45 PM',
        '04:00 PM' => '04:00 PM',
        '04:15 PM' => '04:15 PM',
        '04:30 PM' => '04:30 PM',
        '04:45 PM' => '04:45 PM',
        '05:00 PM' => '05:00 PM',
        '05:15 PM' => '05:15 PM',
        '05:30 PM' => '05:30 PM',
        '05:45 PM' => '05:45 PM',
    ];

    return $slots;
}

function get_blood_groups()
{
    $slots = [
        'A+'  => 'A+',
        'A-'  => 'A-',
        'AB+' => 'AB+',
        'AB-' => 'AB-',
        'B+'  => 'B+',
        'B-'  => 'B-',
        'O+'  => 'O+',
        'O-'  => 'O-'
    ];

    return $slots;
}

/**
 * @param null $return_type
 *
 * @return null
 */
function get_current_session($return_type = NULL)
{
    $CI = &get_instance();

    $session = $CI->session->userdata('new_clinic');

    $return = NULL;

    if ( ! empty($session))
    {
        if (isset($session['patient']))
        {
            if (isset($session['patient']['is_logged_in']) && $session['patient']['is_logged_in'] == TRUE)
            {
                if ( ! empty($return_type))
                {
                    if (isset($session['patient'][$return_type]))
                    {
                        $return = $session['patient'][$return_type];
                    }
                }
                else
                {
                    $return = $session['patient'];
                }
            }
        }
    }

    return $return;
}

function check_patient_session($controller_type = null)
{
    $CI      = &get_instance();
    $session = $CI->session->userdata('new_clinic');

    if($controller_type == 'appointment')
    {
        if ( ! empty($session))
        {
            if ( ! isset($session['patient']))
            {
                redirect(base_url('patient/login'));
            }
            else
            {
                if (isset($session['patient']['is_logged_in']) && $session['patient']['is_logged_in'] != TRUE)
                {
                    redirect(base_url('patient/login'));
                }
            }
        }
        else
        {
            redirect(base_url('patient/login'));
        }
    }
    else if($controller_type == 'patient')
    {
        if ( ! empty($session))
        {
            if (isset($session['patient']))
            {
                if (isset($session['patient']['is_logged_in']) && $session['patient']['is_logged_in'] == TRUE)
                {
                    redirect(base_url('appointment'));
                }
            }
        }
    }
}