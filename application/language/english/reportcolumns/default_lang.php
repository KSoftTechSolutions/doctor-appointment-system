<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

///////////////////////////////
// table headers for product
//////////////////////////////

$lang['department_listing_headers'] = array(
    'checkbox'   => '',
    'dept_name'  => 'Department Name',
    'created_at' => 'Created',
    'updated_at' => 'Modified',
    'edit'       => 'Action',
);

$lang['doctor_listing_headers'] = array(
    'checkbox'   => '',
    'dept_name'  => 'Department Name',
    'name'       => 'Doctor Name',
    'email'      => 'E-mail',
    'mobile'     => 'Mobile',
    'address'    => 'Address',
    'created_at' => 'Created',
    'updated_at' => 'Modified',
    'edit'       => 'Action',
);

$lang['patient_listing_headers'] = array(
    'checkbox'   => '',
    'name'       => 'Name',
    'dob'        => 'DOB',
    'gender'     => 'Gender',
    'email'      => 'E-mail',
    'mobile'     => 'Mobile',
    'address'    => 'Address',
    //    'height'         => 'Height',
    //    'weight'         => 'Weight',
    //    'blood_group'    => 'Blood Group',
    //    'blood_pressure' => 'Blood Pressure',
    //    'past_disease'   => 'Past Disease',
    //    'allergies'      => 'Allergies',
    'created_at' => 'Created',
    'updated_at' => 'Modified',
    'edit'       => 'Action',
);

$lang['appointment_listing_headers'] = array(
    'checkbox'     => '',
    'appnt_no'     => 'Appointment No.',
    'patient_name' => 'Patient Name',
    'booking_date' => 'Booking Date',
    'booking_time' => 'Booking Time',
    'created_at'   => 'Created',
    'updated_at'   => 'Modified',
    'edit'         => 'Action',
);