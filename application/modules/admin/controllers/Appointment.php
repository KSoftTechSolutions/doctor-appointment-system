<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('Department_model', 'Doctor_model', 'Appointment_model', 'Patient_model'),
    );

    /**
     * Appointment constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add, Edit Appointment
     *
     * @param null $appointment_id
     */
    public function add($appointment_id = NULL)
    {
        $this->form_validation->set_rules('dept_id', 'Department Name', "trim|required");
        $this->form_validation->set_rules('name', 'Doctor Name', "trim|required");
        $this->form_validation->set_rules('email', 'Doctor E-mail', "trim|required|valid_email");
        $this->form_validation->set_rules('mobile', 'Doctor Mobile Number', "trim|required");
        $this->form_validation->set_rules('address', 'Doctor Address', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($appointment_id))
            {
                $appointment_details = $this->Appointment_model->get_appointment_detail_by(['id' => $appointment_id]);

                if ( ! empty($appointment_details))
                {
                    $dataArray = array(
                        'dept_id' => $appointment_details['dept_id'],
                        'name'    => $appointment_details['name'],
                        'email'   => $appointment_details['email'],
                        'mobile'  => $appointment_details['mobile'],
                        'address' => $appointment_details['address']
                    );

                    $dataArray['form_action'] = 'Edit Appointment';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/appointment/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Appointment';
            }

            $dataArray['departments'] = $this->Department_model->get_department_detail_by([], 'result_array');

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $dataArray['page_title']     = 'Appointment';
            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('appointment/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'dept_id' => $this->input->post('dept_id'),
                'name'    => $this->input->post('name'),
                'email'   => $this->input->post('email'),
                'mobile'  => $this->input->post('mobile'),
                'address' => $this->input->post('address'),
            );

            if ( ! empty($appointment_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_appointment_id = $this->Appointment_model->save_appointment($params, $appointment_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_appointment_id = $this->Appointment_model->save_appointment($params);
            }

            if ( ! empty($new_appointment_id))
            {
                $this->session->set_flashdata('flash_message', (empty($appointment_id)) ? 'Appointment created successfully' : 'Appointment updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/appointment/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/appointment/list');
            }
        }
    }

    public function appointment_view($appointment_id)
    {
        $appointment_details = [];

        if ( ! empty($appointment_id))
        {
            $appointment_details = $this->Appointment_model->get_appointment_detail_by(['id' => $appointment_id]);

            if ( ! empty($appointment_details))
            {
                $doctor_details  = $this->Doctor_model->get_doctor_detail_by(['id' => $appointment_details['doct_id']]);
                $patient_details = $this->Patient_model->get_patient_detail_by(['id' => $appointment_details['patient_id']]);;

                $appointment_details['doctor_name']     = ! empty($doctor_details) ? $doctor_details['name'] : '';
                $appointment_details['patient_details'] = ! empty($patient_details) ? $patient_details : '';
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/appointment/list'));
            }
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect(base_url('admin/appointment/list'));
        }
//p($appointment_details);
        $dataArray['appointment_details'] = $appointment_details;

        $dataArray['css_local'] = array();
        $dataArray['js_local']  = array();

        $dataArray['page_title'] = 'Appointment Details';

        $dataArray['flash_message']        = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $this->load->view('appointment/print-view', $dataArray);
    }

    /**
     * Appointment View
     */
    public function index()
    {
        $listing_headers = 'appointment_listing_headers';

        $data['source']          = site_url('admin/appointment/listAppointment_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Appointment';
        $data['page_title']      = 'Appointment';

        $dataArray = $this->_table_listing($data);

        $this->load->view('appointment/index', $dataArray);
    }

    /**
     * Appointment DataTable JSON
     */
    public function listAppointment_Json()
    {
        $listing_headers = 'appointment_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Appointment_model->tbl_name = 'appointment';

        $this->Appointment_model->join_tbl_name  = 'department';
        $this->Appointment_model->join_tbl_name1 = 'doctor';
        $this->Appointment_model->join_tbl_name2 = 'patient';
        $this->Appointment_model->join_cond      = "{$this->Appointment_model->tbl_name}.dept_id = {$this->Appointment_model->join_tbl_name}.id";
        $this->Appointment_model->join_cond1     = "{$this->Appointment_model->tbl_name}.doct_id = {$this->Appointment_model->join_tbl_name}.id";
        $this->Appointment_model->join_cond2     = "{$this->Appointment_model->tbl_name}.patient_id = {$this->Appointment_model->join_tbl_name}.id";

        $this->Appointment_model->select_db_cols = "{$this->Appointment_model->tbl_name}.id,
                                                 {$this->Appointment_model->tbl_name}.appnt_no, 
                                                 {$this->Appointment_model->join_tbl_name}.dept_name, 
                                                 {$this->Appointment_model->join_tbl_name1}.name as doctor_name, 
                                                 {$this->Appointment_model->join_tbl_name2}.name as patient_name, 
                                                 {$this->Appointment_model->tbl_name}.booking_date, 
                                                 {$this->Appointment_model->tbl_name}.booking_time, 
                                                 {$this->Appointment_model->tbl_name}.created_at, 
                                                 {$this->Appointment_model->tbl_name}.updated_at";

        $this->Appointment_model->list_search_key  = "{$this->Appointment_model->tbl_name}.appnt_no";
        $this->Appointment_model->list_search_key1 = "{$this->Appointment_model->join_tbl_name}.dept_name";
        $this->Appointment_model->list_search_key2 = "{$this->Appointment_model->join_tbl_name1}.name";
        $this->Appointment_model->list_search_key3 = "{$this->Appointment_model->join_tbl_name2}.name";
        $this->Appointment_model->list_search_key4 = "{$this->Appointment_model->tbl_name}.booking_date";
        $this->Appointment_model->list_search_key5 = "{$this->Appointment_model->tbl_name}.booking_time";
        $this->Appointment_model->list_search_key6 = "{$this->Appointment_model->tbl_name}.created_at";
        $this->Appointment_model->list_search_key7 = "{$this->Appointment_model->tbl_name}.updated_at";

        $resultdata = $this->Appointment_model->get_all_appointment_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Appointment
     *
     * @param $appointment_id
     */
    public function delete($appointment_id)
    {
        $res = $this->Appointment_model->delete_appointment(['id' => $appointment_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/appointment/list');
    }
}