<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('Department_model'),
    );

    /**
     * Department constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add, Edit Department
     *
     * @param null $department_id
     */
    public function add($department_id = NULL)
    {
        $this->form_validation->set_rules('dept_name', 'Department Name', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($department_id))
            {
                $department_details = $this->Department_model->get_department_detail_by(['id' => $department_id]);

                if ( ! empty($department_details))
                {
                    $dataArray = array(
                        'dept_name' => $department_details['dept_name'],
                    );

                    $dataArray['form_action'] = 'Edit Department';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/department/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Department';
            }

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $dataArray['page_title']     = 'Department';
            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('department/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'dept_name' => $this->input->post('dept_name'),
            );

            if ( ! empty($department_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_department_id = $this->Department_model->save_department($params, $department_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_department_id = $this->Department_model->save_department($params);
            }

            if ( ! empty($new_department_id))
            {
                $updated_department_id = $this->Department_model->save_department(['dept_id' => 'DEPT-00' . $new_department_id], $new_department_id);

                $this->session->set_flashdata('flash_message', (empty($department_id)) ? 'Department created successfully.' : 'Department updated successfully.');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/department/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/department/list');
            }
        }
    }

    /**
     * Department View
     */
    public function index()
    {
        $listing_headers = 'department_listing_headers';

        $data['source']          = site_url('admin/department/listDepartment_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Department';
        $data['page_title']      = 'Department';

        $dataArray = $this->_table_listing($data);

        $this->load->view('department/index', $dataArray);
    }

    /**
     * Department DataTable JSON
     */
    public function listDepartment_Json()
    {
        $listing_headers = 'department_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Department_model->tbl_name       = 'department';
        $this->Department_model->select_db_cols = "id, dept_id, dept_name, created_at, updated_at";

        $this->Department_model->list_search_key  = 'dept_name';
        $this->Department_model->list_search_key1 = 'created_at';
        $this->Department_model->list_search_key2 = 'updated_at';

        $resultdata = $this->Department_model->get_all_department_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Department
     *
     * @param $department_id
     */
    public function delete($department_id)
    {
        $res = $this->Department_model->delete_department(['id' => $department_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/department/list');
    }
}