<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('Patient_model'),
    );

    /**
     * Patient constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add, Edit Patient
     *
     * @param null $patient_id
     */
    public function add($patient_id = NULL)
    {
        $this->form_validation->set_rules('name', 'Patient Name', "trim|required");
        $this->form_validation->set_rules('dob', 'Date of Birth', "trim|required");
        $this->form_validation->set_rules('gender', 'Gender', "trim|required");
        $this->form_validation->set_rules('email', 'E-mail', "trim|required|valid_email");
        $this->form_validation->set_rules('mobile', 'Mobile Number', "trim|required");
        $this->form_validation->set_rules('address', 'Address', "trim|required");
        $this->form_validation->set_rules('state', 'State', "trim|required");
        $this->form_validation->set_rules('city', 'City', "trim|required");
        $this->form_validation->set_rules('pin_code', 'Pin Code', "trim|required");
        $this->form_validation->set_rules('height', 'Height', "trim|required");
        $this->form_validation->set_rules('weight', 'Weight', "trim|required");
        $this->form_validation->set_rules('blood_group', 'Blood Group', "trim|required");
        $this->form_validation->set_rules('blood_pressure', 'Blood Pressure', "trim|required");
        $this->form_validation->set_rules('past_disease', 'Any Past Disease', "trim|required");
        $this->form_validation->set_rules('allergies', 'Any Allergies', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($patient_id))
            {
                $patient_details = $this->Patient_model->get_patient_detail_by(['id' => $patient_id]);

                if ( ! empty($patient_details))
                {
                    $dataArray = array(
                        'name'           => $patient_details['name'],
                        'dob'            => $patient_details['dob'],
                        'gender'         => $patient_details['gender'],
                        'email'          => $patient_details['email'],
                        'mobile'         => $patient_details['mobile'],
                        'address'        => $patient_details['address'],
                        'state'          => $patient_details['state'],
                        'city'           => $patient_details['city'],
                        'pin_code'       => $patient_details['pin_code'],
                        'height'         => $patient_details['height'],
                        'weight'         => $patient_details['weight'],
                        'blood_group'    => $patient_details['blood_group'],
                        'blood_pressure' => $patient_details['blood_pressure'],
                        'past_disease'   => $patient_details['past_disease'],
                        'allergies'      => $patient_details['allergies'],
                    );

                    $dataArray['form_action'] = 'Edit Patient';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/patient/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Patient';
            }

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $dataArray['page_title']     = 'Patient';
            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('patient/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'name'           => $this->input->post('name'),
                'dob'            => $this->input->post('dob'),
                'gender'         => $this->input->post('gender'),
                'email'          => $this->input->post('email'),
                'mobile'         => $this->input->post('mobile'),
                'address'        => $this->input->post('address'),
                'state'          => $this->input->post('state'),
                'city'           => $this->input->post('city'),
                'pin_code'       => $this->input->post('pin_code'),
                'height'         => $this->input->post('height'),
                'weight'         => $this->input->post('weight'),
                'blood_group'    => $this->input->post('blood_group'),
                'blood_pressure' => $this->input->post('blood_pressure'),
                'past_disease'   => $this->input->post('past_disease'),
                'allergies'      => $this->input->post('allergies'),
            );

            if ( ! empty($patient_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_patient_id = $this->Patient_model->save_patient($params, $patient_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_patient_id = $this->Patient_model->save_patient($params);
            }

            if ( ! empty($new_patient_id))
            {
                $updated_patient_id = $this->Patient_model->save_patient(['patient_id' => 'P-00' . $new_patient_id], $new_patient_id);

                $this->session->set_flashdata('flash_message', (empty($patient_id)) ? 'Patient created successfully.' : 'Patient updated successfully.');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/patient/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/patient/list');
            }
        }
    }

    /**
     * Patient View
     */
    public function index()
    {
        $listing_headers = 'patient_listing_headers';

        $data['source']          = site_url('admin/patient/listPatient_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Patient';
        $data['page_title']      = 'Patient';

        $dataArray = $this->_table_listing($data);

        $this->load->view('patient/index', $dataArray);
    }

    /**
     * Patient DataTable JSON
     */
    public function listPatient_Json()
    {
        $listing_headers = 'patient_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Patient_model->tbl_name = 'patient';

        $this->Patient_model->select_db_cols = "id, patient_id, name, gender, email, mobile, dob, height, 
        CONCAT_WS(', ', address, state, city, pin_code) as address,
        weight, blood_group, blood_pressure, past_disease, allergies, created_at, updated_at";

        $this->Patient_model->list_search_key   = 'name';
        $this->Patient_model->list_search_key1  = 'dob';
        $this->Patient_model->list_search_key2  = 'email';
        $this->Patient_model->list_search_key3  = 'mobile';
        $this->Patient_model->list_search_key4  = 'gender';
        $this->Patient_model->list_search_key5  = 'height';
        $this->Patient_model->list_search_key6  = 'weight';
        $this->Patient_model->list_search_key7  = 'blood_group';
        $this->Patient_model->list_search_key8  = 'allergies';
        $this->Patient_model->list_search_key9  = 'past_disease';
        $this->Patient_model->list_search_key10 = 'address';
        $this->Patient_model->list_search_key11 = 'state';
        $this->Patient_model->list_search_key12 = 'city';
        $this->Patient_model->list_search_key13 = 'pin_code';
        $this->Patient_model->list_search_key14 = 'created_at';
        $this->Patient_model->list_search_key15 = 'updated_at';

        $resultdata = $this->Patient_model->get_all_patient_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Patient
     *
     * @param $patient_id
     */
    public function delete($patient_id)
    {
        $res = $this->Patient_model->delete_patient(['id' => $patient_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/patient/list');
    }
}