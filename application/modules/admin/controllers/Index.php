<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Index()
    {
        $dataArray['page_title']  = 'Dashboard';
        $dataArray['form_action'] = 'Dashboard';

        $this->load->view('dashboard', $dataArray);
    }
}