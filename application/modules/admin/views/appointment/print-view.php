<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url('assets/css/module/admin/print.css') ?>"/>
<section class="card">
    <div id="invoice-template" class="card-block">
        <!-- Invoice Company Details -->
        <div id="invoice-company-details" class="row">
            <div class="col-md-6 col-sm-12 text-xs-center text-md-left">
                <i class="fa fa-stethoscope" style="font-size: 30px"></i>
                <ul class="px-0 list-unstyled">
                    <li class="text-bold-800">New Clinic</li>
                    <li>4025 Oak Avenue,</li>
                    <li>Melbourne,</li>
                    <li>Florida 32940,</li>
                    <li>USA</li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-12 text-xs-center text-md-right">
                <h2>Appointment Details</h2>
                <p class="pb-3"><?php echo ! empty($appointment_details) ? '#' . $appointment_details['appnt_no'] : ''; ?></p>
            </div>
        </div>
        <!--/ Invoice Company Details -->

        <!-- Invoice Customer Details -->
        <?php
        if ( ! empty($appointment_details))
        {
            ?>
            <div id="invoice-customer-details" class="row pt-2">
                <div class="col-sm-12 text-xs-center text-md-left">
                    <p class="text-muted text-xs-center">Patient Details</p>
                    <hr>
                </div>
                <div class="col-md-6 col-sm-12 text-xs-center text-md-left">
                    <ul class="px-0 list-unstyled">
                        <li class="text-bold-800"><?php echo $appointment_details['patient_details']['name']; ?></li>
                        <li><?php echo $appointment_details['patient_details']['address']; ?></li>
                        <li><?php echo $appointment_details['patient_details']['state'] . ' ' . $appointment_details['patient_details']['city'] ?></li>
                        <li><?php echo $appointment_details['patient_details']['pin_code']; ?></li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-12 text-xs-center text-md-right">
                    <p><span class="text-muted">Doctor Name :</span> <?php echo $appointment_details['doctor_name']; ?></p>
                    <p><span class="text-muted">Appointment Date :</span> <?php echo date('d-m-Y', strtotime($appointment_details['booking_date'])); ?></p>
                    <p><span class="text-muted">Appointment Time :</span> <?php echo $appointment_details['booking_time']; ?></p>
                </div>
            </div>
            <!--/ Invoice Customer Details -->

            <!-- Invoice Items Details -->
            <div id="invoice-items-details" class="pt-2">
                <div class="row">
                    <div class="table-responsive col-sm-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Height</th>
                                <th>Weight</th>
                                <th>Blood Group</th>
                                <th>Blood Pressure</th>
                                <th>Past Disease</th>
                                <th>Allergies</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?php echo $appointment_details['patient_details']['height']; ?></td>
                                <td><?php echo $appointment_details['patient_details']['weight']; ?></td>
                                <td><?php echo $appointment_details['patient_details']['blood_group']; ?></td>
                                <td><?php echo $appointment_details['patient_details']['blood_pressure']; ?></td>
                                <td><?php echo $appointment_details['patient_details']['past_disease']; ?></td>
                                <td><?php echo $appointment_details['patient_details']['allergies']; ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Invoice Footer -->
            <div id="invoice-footer">
                <div class="row">
                    <div class="col-md-7 col-sm-12">
                        <h6></h6>
                        <p></p>
                    </div>
                    <div class="col-sm-12 text-xs-center">
                        <button type="button" class="btn btn-primary btn-lg my-1 printButton" onclick="window.print();"><i class="icon-printer3"></i> Print</button>
                    </div>
                </div>
            </div>
            <!--/ Invoice Footer -->
            <?php
        }
        else
        {
            echo '<p class="text-muted text-xs-center">No Data Found</p>';
        } ?>

    </div>
</section>