<?php
    if ( ! empty($flash_message))
    {
        ?>
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $flash_message; ?>
                </div>
            </div>
        </div>
        <?php
    }
    if ( ! empty($validation_err))
    {
        ?>
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="alert alert-danger alert-dismissible fade in mb-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $validation_err; ?>
                </div>
            </div>
        </div>
        <?php
    }
?>

<!--jquery validation error container-->
<div id="errorContainer" class="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p>Please correct the following errors and try again:</p>
    <ul></ul>
</div>

<section class="card">
    <div class="card-header">
        <h2 class="card-title"><?php echo ! empty($form_action) ? $form_action : ''; ?></h2>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                <li>
                    <a href="<?php echo base_url('admin/doctor/list'); ?>" class="btn btn-primary btn-block btn-min-width mr-1 mb-1" role="button">
                        <i class="fa fa-list" style="color: white;"></i>&nbsp;
                        List Doctor
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <form id="save_form" action="<?php echo current_url(); ?>" method="post">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Department Name</strong></label>
                        <div class="col-lg-4">
                            <select id="dept_id" name="dept_id" class="form-control" required data-msg-required="Department Name Required">
                                <option value="">Select Department</option>
                                <?php
                                if ( ! empty($departments))
                                {
                                    foreach ($departments as $dept)
                                    {
                                        $selected = ($dept['id'] == (! empty($dept_id) ? $dept_id : $this->input->post('dept_id')) ? 'selected' : '');
                                        echo '<option value="' . $dept['id'] . '"' . $selected . '>' . $dept['dept_name'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Doctor Name</strong></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="name" id="name" autocomplete="off"
                                   value="<?php echo ! empty($name) ? $name : $this->input->post('name'); ?>"
                                   placeholder="Enter Name" required data-msg-required="Doctor Name Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Doctor E-mail</strong></label>
                        <div class="col-lg-4">
                            <input type="email" class="form-control" name="email" id="email" autocomplete="off"
                                   value="<?php echo ! empty($email) ? $email : $this->input->post('email'); ?>"
                                   placeholder="Enter E-mail" required data-msg-required="Doctor E-mail Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Doctor Mobile</strong></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="mobile" id="mobile" autocomplete="off"
                                   value="<?php echo ! empty($mobile) ? $mobile : $this->input->post('mobile'); ?>" maxlength="10"
                                   placeholder="Enter Mobile Number" required data-msg-required="Doctor Mobile Number Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Doctor Address</strong></label>
                        <div class="col-lg-4">
                            <textarea class="form-control" placeholder="Enter Address" name="address" id="address" autocomplete="off" required data-msg-required="Doctor Address Required"><?php
                                echo ! empty($address) ? $address : $this->input->post('address');
                            ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"></label>
                        <div class="col-lg-4">
                            <div class="text-right">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>