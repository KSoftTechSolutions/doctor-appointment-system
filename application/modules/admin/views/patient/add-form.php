<?php
if ( ! empty($flash_message))
{
    ?>
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
    <?php
}
if ( ! empty($validation_err))
{
    ?>
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="alert alert-danger alert-dismissible fade in mb-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<!--jquery validation error container-->
<div id="errorContainer" class="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p>Please correct the following errors and try again:</p>
    <ul></ul>
</div>

<section class="card">
    <div class="card-header">
        <h2 class="card-title"><?php echo ! empty($form_action) ? $form_action : ''; ?></h2>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                <li>
                    <a href="<?php echo base_url('admin/patient/list'); ?>" class="btn btn-primary btn-block btn-min-width mr-1 mb-1" role="button">
                        <i class="fa fa-list" style="color: white;"></i>&nbsp;
                        List Patient
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <form id="save_form" action="<?php echo current_url(); ?>" method="post">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Department Name</strong></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="dept_name" id="dept_name" autocomplete="off"
                                   value="<?php echo ! empty($dept_name) ? $dept_name : $this->input->post('dept_name'); ?>"
                                   placeholder="Enter Department Name" required data-msg-required="Department Name Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"></label>
                        <div class="col-lg-4">
                            <div class="text-right">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>