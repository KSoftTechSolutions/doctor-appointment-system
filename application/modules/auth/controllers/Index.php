<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{

    public $autoload = array(
        'libraries' => array('form_validation'),
    );

    /**
     * Admin Login View & Validate
     */
    public function login()
    {
        $this->form_validation->set_rules('uemail', 'Username', 'required|max_length[255]');
        $this->form_validation->set_rules('upass', 'Password', 'required');

        if ($this->form_validation->run())
        {
            $data = array(
                'uemail' => $this->input->post('uemail'),
                'upass'  => $this->input->post('upass'),
            );

            $admin_cred_config = $this->config->item('app_config');

            if (($data['uemail'] == $admin_cred_config['admin_user']) && ($admin_cred_config['admin_pass'] == password_decrypt($data['upass'], $admin_cred_config['admin_pass'])))
            {
                $this->session->set_userdata('user_auth', array(
                    'connected' => TRUE,
                    'id'        => 1
                ));

                redirect(base_url('admin/dashboard'));
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Error, Please provide valid login credential.');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/login'));
            }
        }
        else
        {
            $validation_errors = validation_errors();
            if ( ! empty($validation_errors))
            {
                $this->session->set_flashdata('flash_message', $validation_errors);
                $this->session->set_flashdata('flash_message_status', FALSE);
            }

            $data['flash_message']        = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->setTemplate('login');
            $this->load->view('login', $data);
        }
    }

    /**
     * Logout Admin
     */
    public function logout()
    {
        $this->session->sess_destroy();

        redirect(base_url('admin/login'));
    }

    /**
     * Forgot Password View & Validation
     */
    public function forgot_password()
    {
        if ($this->form_validation->run())
        {

        }
        else
        {
            $validation_errors = validation_errors();
            if ( ! empty($validation_errors))
            {
                $this->session->set_flashdata('flash_message', $validation_errors);
                $this->session->set_flashdata('flash_message_status', FALSE);
            }

            $data['flash_message']        = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->setTemplate('login');
            $this->load->view('recover-password', $data);
        }
    }

    /**
     * Reset Password View & Validation
     */
    public function reset_password()
    {
        if ($this->form_validation->run())
        {

        }
        else
        {
            $validation_errors = validation_errors();
            if ( ! empty($validation_errors))
            {
                $this->session->set_flashdata('flash_message', $validation_errors);
                $this->session->set_flashdata('flash_message_status', FALSE);
            }

            $data['flash_message']        = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->setTemplate('login');
            $this->load->view('reset-password', $data);
        }
    }
}