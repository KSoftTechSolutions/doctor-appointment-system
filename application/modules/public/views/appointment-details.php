<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url('assets/css/module/public/print.css') ?>"/>

<div class="w3ls-banner">
    <div class="heading">
        <h1>Appointment Details
            <hr>
        </h1>
    </div>
    <div class="container_1">
        <div class="heading">
            <h2><?php echo ( ! empty($patient_details) && ! empty($appointment_details)) ? 'Please print a copy of this appointment' : ''; ?></h2>
            <p></p>
            <?php
            if ( ! empty($flash_message))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $flash_message; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            if ( ! empty($validation_err))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert alert-danger alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $validation_err; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <!--jquery validation error container-->
            <div id="errorContainer" class="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p class="text-left">Please correct the following errors and try again:</p>
                <ul class="text-left" style="list-style: disc!important;"></ul>
            </div>
        </div>

        <div class="agile-form">
            <ul class="field-list" style="<?php echo ( ! empty($patient_details) && ! empty($appointment_details)) ? 'margin-left: 100px' : ''; ?>">
                <?php

                $style = 'display:none';

                if ( ! empty($patient_details) && ! empty($appointment_details))
                {
                    $style = '';
                    ?>
                    <li>
                        <label class="form-label">
                            Appointment No:
                        </label>
                        <label class="form-label">
                            <?php echo $appointment_details['appnt_no']; ?>
                        </label>
                    </li>
                    <li>
                        <label class="form-label">
                            Patient Name:
                        </label>
                        <label class="form-label">
                            <?php echo $patient_details['name']; ?>
                        </label>
                    </li>
                    <li>
                        <label class="form-label">
                            Doctor Name:
                        </label>
                        <label class="form-label">
                            <?php echo $appointment_details['doctor_name']; ?>
                        </label>
                    </li>
                    <li>
                        <label class="form-label">
                            Appointment Date:
                        </label>
                        <label class="form-label">
                            <?php echo date('d-m-Y', strtotime($appointment_details['booking_date'])); ?>
                        </label>
                    </li>
                    <li>
                        <label class="form-label">
                            Appointment Time:
                        </label>
                        <label class="form-label">
                            <?php echo $appointment_details['booking_time']; ?>
                        </label>
                    </li>
                    <?php
                }
                else
                {
                    echo '<li class="text-center">No Data found</li>';
                }
                ?>
            </ul>
            <br>
            <input type="submit" class="printButton" onclick="window.print();" value="Print" style="<?php echo $style; ?>">
        </div>
    </div>
</div>