<div class="w3ls-banner">
    <div class="heading">
        <h1>Patient Login<hr></h1>
    </div>
    <div class="container_1">
        <div class="heading">
            <h2></h2>
            <p></p>
            <?php
            if ( ! empty($flash_message))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $flash_message; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            if ( ! empty($validation_err))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert alert-danger alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $validation_err; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <!--jquery validation error container-->
            <div id="errorContainer" class="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p class="text-left">Please correct the following errors and try again:</p>
                <ul class="text-left" style="list-style: disc!important;"></ul>
            </div>
        </div>
        <div class="agile-form">
            <form action="" method="post" id="save_form">
                <ul class="field-list">
                    <li>
                        <label class="form-label">
                            Mobile Number
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <input type="text" name="mobile_number" id="mobile_number" maxlength="10" placeholder="Enter Mobile Number" autocomplete="off" required data-msg-required="Mobile Number Required">
                        </div>
                    </li>
                    <li>
                        <label class="form-label">
                            Date of Birth
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <input type="text" name="date_of_birth" id="date_of_birth" maxlength="10" placeholder="Enter Date of Birth" autocomplete="off" required data-msg-required="Date of Birth Required">
                        </div>
                    </li>
                </ul>
                <br>
                <input type="submit" value="Login">
            </form>
        </div>
    </div>
</div>