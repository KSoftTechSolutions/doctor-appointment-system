<div class="w3ls-banner">
    <div class="heading">
        <h1>Appoint My Doctor<hr></h1>
    </div>
    <div class="container_1">
        <div class="heading">
            <h2>Please Enter Patients Details</h2>
            <p>Fill the form below and submit your query we will contact you as soon as possible.</p>
            <?php
            if ( ! empty($flash_message))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $flash_message; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            if ( ! empty($validation_err))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert alert-danger alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $validation_err; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <!--jquery validation error container-->
            <div id="errorContainer" class="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p class="text-left">Please correct the following errors and try again:</p>
                <ul class="text-left" style="list-style: disc!important;"></ul>
            </div>
        </div>
        <div class="agile-form">
            <form action="" method="post" id="save_form">
                <ul class="field-list">
                    <li>
                        <label class="form-label">
                            Department Name
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <select id="dept_id" name="dept_id" class="form-dropdown" required data-msg-required="Department Name Required">
                                <option value="">Select Department</option>
                                <?php
                                if ( ! empty($departments))
                                {
                                    foreach ($departments as $dept)
                                    {
                                        $selected = ($dept['id'] == (! empty($dept_id) ? $dept_id : $this->input->post('dept_id')) ? 'selected' : '');
                                        echo '<option value="' . $dept['id'] . '"' . $selected . '>' . $dept['dept_name'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </li>
                    <li>
                        <label class="form-label">
                            Doctor Name
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <select id="doctor_id" name="doctor_id" class="form-dropdown" required data-msg-required="Doctor Name Required">
                                <option value="">Select Doctor</option>
                            </select>
                        </div>
                    </li>
                    <li>
                        <label class="form-label">
                            Date of Appointment
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <input type="text" name="appointment_date" id="appointment_date" placeholder="Enter Date" required data-msg-required="Date of Appointment Required">
                        </div>
                    </li>
                    <li>
                        <label class="form-label">
                            Time of Appointment
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <select name="appointment_time" id="appointment_time" class="form-dropdown" required data-msg-required="Appointment Time Required">
                                <option value="">Select Time of Appointment</option>
                                <?php
                                if ( ! empty($time_slots))
                                {
                                    foreach ($time_slots as $slot_k => $slot_v)
                                    {
                                        $selected = '';
                                        echo '<option value="' . $slot_k . '"' . $selected . '>' . $slot_v . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </li>
                </ul>
                <br>
                <input type="submit" value="Book Appointment">
            </form>
        </div>
    </div>
</div>