<?php defined('BASEPATH') OR exit('No direct script access allowed');

class App_model extends MY_Model
{
    public $tbl_name = 'department';

    /**
     * App_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null  $tbl_name
     * @param array $params
     * @param array $condition
     *
     * @return null
     */
    function save_details($tbl_name = NULL, $params = [], $condition = [])
    {
        $this->tbl_name = ! empty($tbl_name) ? $tbl_name : $this->tbl_name;

        $return = NULL;

        if ( ! empty($condition))
        {
            $return = $this->db->update($this->tbl_name, $params, $condition);
        }
        else
        {
            $this->db->insert($this->tbl_name, $params);
            $return = $this->db->insert_id();
        }

        return $return;
    }

    /**
     * @param null   $tbl_name
     * @param array  $params
     * @param string $return_type
     *
     * @return null
     */
    function get_detail_by($tbl_name = NULL, $params = [], $return_type = 'row_array')
    {
        $this->tbl_name = ! empty($tbl_name) ? $tbl_name : $this->tbl_name;

        $result = NULL;

        if ( ! empty($params))
        {
            if ($return_type == 'result_array')
            {
                $result = $this->db->get_where($this->tbl_name, $params)->result_array();
            }
            else
            {
                $result = $this->db->get_where($this->tbl_name, $params)->row_array();
            }
        }
        else
        {
            if ($return_type == 'result_array')
            {
                $result = $this->db->get($this->tbl_name)->result_array();
            }
            else
            {
                $result = $this->db->get($this->tbl_name)->row_array();
            }
        }

        return $result;
    }
}
