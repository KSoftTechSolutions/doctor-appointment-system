<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{
    /**
     * Index constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Redirect to Home URL
     */
    public function Index()
    {
        redirect(base_url('home'));
    }

    /**
     * Home view
     */
    public function home()
    {
        $dataArray['page_title'] = 'Home';

        $dataArray['css_local'] = [];
        $dataArray['js_local']  = [];

        $this->load->view('index', $dataArray);
    }

    /**
     * About Us view
     */
    public function about_us()
    {
        $dataArray['page_title']  = 'About Us';
        $dataArray['header_type'] = 'header2';

        $dataArray['css_local'] = [];
        $dataArray['js_local']  = [];

        $this->load->view('about-us', $dataArray);
    }

    /**
     * Departments view
     */
    public function department()
    {
        $dataArray['page_title']  = 'Departments';
        $dataArray['header_type'] = 'header2';

        $dataArray['css_local'] = [];
        $dataArray['js_local']  = [];

        $this->load->view('departments', $dataArray);
    }

    /**
     * Contact Us view
     */
    public function contact_us()
    {
        $dataArray['page_title']  = 'Contact Us';
        $dataArray['header_type'] = 'header2';

        $dataArray['css_local'] = [];
        $dataArray['js_local']  = [];

        $this->load->view('contact-us', $dataArray);
    }

    /**
     * Under Construction view
     */
    public function under_construction()
    {
        $dataArray['page_title']  = 'Under Construction';
        $dataArray['header_type'] = 'header2';

        $dataArray['css_local'] = [];
        $dataArray['js_local']  = [];

        $this->load->view('under-construction', $dataArray);
    }
}