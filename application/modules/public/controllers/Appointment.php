<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends My_Controller
{
    public $autoload = array(
        'libraries' => array('form_validation'),
        'model'     => array('App_model'),
    );

    /**
     * Appointment constructor.
     */
    public function __construct()
    {
        parent::__construct();

        check_patient_session('appointment');
    }

    /**
     * Appointment
     */
    public function index()
    {
        $this->form_validation->set_rules('dept_id', 'Department Name', "trim|required");
        $this->form_validation->set_rules('doctor_id', 'Doctor Name', "trim|required");
        $this->form_validation->set_rules('appointment_date', 'Appointment Date', "trim|required");
        $this->form_validation->set_rules('appointment_time', 'Appointment Time', "trim|required");

        if ( ! $this->form_validation->run())
        {
            $dataArray['departments']  = $this->App_model->get_detail_by('department', [], 'result_array');
            $dataArray['time_slots']   = get_appointment_slots();
            $dataArray['blood_groups'] = get_blood_groups();

            $dataArray['page_title']  = 'Appointment';
            $dataArray['header_type'] = 'header2';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $dataArray['css_local'] = ['bootstrap-datepicker'];
            $dataArray['js_local']  = [
                'jquery.validate',
                'validate-additional-methods',
                'alphanumeric',
                'bootstrap-datepicker',
                'appointment',
            ];

            $this->load->view('appointment', $dataArray);
        }
        else
        {
            $appointment_date = $this->input->post('appointment_date');

            $params = [
                'dept_id'      => $this->input->post('dept_id'),
                'doct_id'      => $this->input->post('doctor_id'),
                'patient_id'   => get_current_session('id'),
                'booking_date' => date('Y-m-d', strtotime($appointment_date)),
                'booking_time' => $this->input->post('appointment_time'),
                'created_at'   => date('Y-m-d H:i:s'),
                'updated_at'   => date('Y-m-d H:i:s'),
            ];

            $new_appointment_id = $this->App_model->save_details('appointment', $params);

            if ( ! empty($new_appointment_id))
            {
                $patient_id_status = $this->App_model->save_details('appointment', ['appnt_no' => 'APPN-00' . $new_appointment_id], ['id' => $new_appointment_id]);

                $this->session->set_flashdata('flash_message', 'Your appointment created successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect(base_url('appointment-detail/' . $new_appointment_id));
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('appointment'));
            }
        }
    }

    public function appointment_detail($appointment_id)
    {
        $appointment_details = [];

        if ( ! empty($appointment_id))
        {
            $appointment_details = $this->App_model->get_detail_by('appointment', ['id' => $appointment_id, 'patient_id' => get_current_session('id')]);

            if ( ! empty($appointment_details))
            {
                $doctor_details = $this->App_model->get_detail_by('doctor', ['id' => $appointment_details['doct_id']]);

                $appointment_details['doctor_name'] = ! empty($doctor_details) ? $doctor_details['name'] : '';
            }
            else
            {
                $appointment_details = [];
            }
        }

        $dataArray['appointment_details'] = $appointment_details;
        $dataArray['patient_details']     = $patient_details = $this->App_model->get_detail_by('patient', ['id' => get_current_session('id')]);;

        if (empty($patient_details))
        {
            redirect(base_url('patient/login'));
        }

        $dataArray['page_title']  = 'Appointment Details';
        $dataArray['header_type'] = 'header2';

        $dataArray['validation_err'] = validation_errors();

        $dataArray['flash_message']        = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $dataArray['css_local'] = [];
        $dataArray['js_local']  = [];

        $this->load->view('appointment-details', $dataArray);
    }

    /**
     * Get Doctors by Department AJAX
     */
    public function get_doctor_by_department()
    {
        $dept_id = $this->input->post('dept_id');

        if ( ! empty($dept_id))
        {
            $doctors = $this->App_model->get_detail_by('doctor', ['dept_id' => $dept_id], 'result_array');

            if ( ! empty($doctors))
            {
                echo json_encode(['status' => 'true', 'data' => $doctors]);
            }
            else
            {
                echo json_encode(['status' => 'false']);
            }
        }
        else
        {
            echo json_encode(['status' => 'false']);
        }
    }
}