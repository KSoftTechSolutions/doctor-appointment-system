<?php

/*
 * Contributor - Suraj Gupta
 * Email - suraj.gupta.489@gmail.com
 */

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Class MY_Form_validation
 */
class MY_Form_validation extends CI_Form_validation
{

    /**
     * MY_Form_validation constructor.
     *
     * @param array $rules
     */
    public function __construct($rules = array())
    {
        parent::__construct($rules);
        $this->CI->lang->load('MY_form_validation');
    }

    /**
     * @param $password
     *
     * @return string
     */
    public function password_encryption($password)
    {
        $cost = 10;
        $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
        $salt = sprintf("$2a$%02d$", $cost) . $salt;
        $hash = crypt($password, $salt);

        return $hash;
    }

    /**
     * @param $str
     * @param $field
     *
     * @return bool
     */
    public function is_unique_modified($str, $field)
    {
        list($table, $field, $id, $idVal) = explode('.', $field);
        $query = $this->CI->db->limit(1)->get_where($table, array($field => $str, "$id !=" => $idVal));

        return $query->num_rows() === 0;
    }

    /**
     * @param $subject
     * @param $str
     *
     * @return bool
     */
    public function is_equal($subject, $str)
    {
        return $subject == $this->CI->input->post($str);
    }

    /**
     * @param $subject
     *
     * @return bool
     */
    public function alpha_dash_mod($subject)
    {
        $pattern = '/^(?=.{6,20}$)(?![_.\d])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/';
        preg_match($pattern, $subject, $matches);

        return ! empty($matches);
    }

    /**
     * @param $str
     * @param $field
     *
     * @return bool
     */
    public function unique($str, $field)
    {
        $arr = explode('.', $field);

        $table  = $arr[0];
        $column = $arr[1];

        $exclusionField = $exclusionFieldValue = $extraCondition = '';

        if (count($arr) == 4)
        {
            $exclusionField      = $arr[2];
            $exclusionFieldValue = $arr[3];


            if ( ! empty($exclusionField) && ! empty($exclusionFieldValue))
            {
                $extraCondition = " AND $exclusionField <> '$exclusionFieldValue'";
            }
        }
        if (count($arr) == 6)
        {
            $exclusionField1      = $arr[2];
            $exclusionFieldValue1 = $arr[3];
            $exclusionField2      = $arr[4];
            $exclusionFieldValue2 = $arr[5];
            if (( ! empty($exclusionField1) && ! empty($exclusionFieldValue1)) || ( ! empty($exclusionFieldValue2) && ! empty($exclusionFieldValue2)))
            {
                $extraCondition = " AND $exclusionField1 <> '$exclusionFieldValue1' AND $exclusionField2 = '$exclusionFieldValue2'";
            }
        }

        $this->CI->form_validation->set_message('unique', '%s already exists.');
        $query = $this->CI->db->query("SELECT COUNT(*) AS duplicate FROM $table WHERE $column = " . $this->CI->db->escape($str) . " $extraCondition");
        $row   = $query->row();

        return ($row->duplicate > 0) ? FALSE : TRUE;
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public function valid_date($date)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);

        return $d && $d->format('Y-m-d') === $date;
    }

    /**
     * @param $date
     *
     * @return bool
     */
    public function valid_date_time($date)
    {
        $d = DateTime::createFromFormat('Y-m-d H:i:s', $date);

        return $d && $d->format('Y-m-d H:i:s') === $date;
    }

    /**
     * @param $str
     * @param $field
     *
     * @return bool
     */
    public function password_check($str, $field)
    {

        $cnf_password = $field;
        $password     = $str;

        $return = FALSE;
        if ($password == $cnf_password)
        {
            $return = TRUE;
        }

        return $return;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function is_pos($data)
    {

        if ($data >= 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * @param $controlName
     * @param $module
     *
     * @return bool
     */
    public function validate_file_upload($controlName, $module)
    {
        $myconfig = getCustomConfigItem($module);

        if ( ! is_dir($myconfig['upload_path']))
        {
            mkdir($myconfig['upload_path'], 0755, TRUE);
        }

        $this->_CI->load->library('upload');

        $this->_CI->upload->initialize($myconfig);

        $message = '';

        if ( ! $this->_CI->upload->dry_run_upload($controlName))
        {
            $message = $this->_CI->upload->display_errors();
            $this->_CI->form_validation->set_message('validate_file_upload', $message);
        }

        return empty($message);
    }

    /**
     * @param $pass
     *
     * @return bool
     */
    public function password_validation_check($pass)
    {
        $password_length_check = strlen($pass);
        $alpha_numeric_check   = ctype_alnum($pass);
        $caps_check            = preg_match('/[A-Z]+/', $pass);

        if ($password_length_check < 8)
        {
            return FALSE;
        }
        else if ($alpha_numeric_check == FALSE)
        {
            return FALSE;
        }
        else if ($caps_check == FALSE)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }

    }

    /**
     * @param $password
     * @param $check_password
     *
     * @return bool
     */
    public function check_password($password, $check_password)
    {
        if ($password == $check_password)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Validation check only for country, state, location table
     *
     * @param $post_value
     * @param $fields
     *
     * @return bool
     */
    public function exists($post_value, $fields)
    {
        $arr = explode('.', $fields);

        $condition = '';

        $table       = $arr[0];
        $post_column = $arr[1];

        if (count($arr) == 6)
        {
            $where_column  = $arr[2];
            $where_value   = $arr[3];
            $where_column1 = $arr[4];
            $where_value1  = $arr[5];

            if ( ! empty($where_value1))
            {
                $condition = "WHERE $where_column = $where_value AND $where_column1 <> $where_value1 AND $post_column = '$post_value'";
            }
            else
            {
                $condition = "WHERE $where_column = $where_value AND $post_column = '$post_value'";
            }
        }
        else if (count($arr) == 8)
        {
            $where_column  = $arr[2];
            $where_value   = $arr[3];
            $where_column1 = $arr[4];
            $where_value1  = $arr[5];
            $where_column2 = $arr[6];
            $where_value2  = $arr[7];

            if ( ! empty($where_value2))
            {
                $condition = "WHERE $where_column = $where_value AND $where_column1 = $where_value1 AND $where_column2 <> $where_value2 AND $post_column = '$post_value'";
            }
            else
            {
                $condition = "WHERE $where_column = $where_value AND $where_column1 = $where_value1 AND $post_column = '$post_value'";
            }
        }

        $this->CI->form_validation->set_message('exists', '%s already exists');

        $query = $this->CI->db->query("SELECT * FROM $table $condition");

        $row = $query->num_rows();

        return ($row > 0) ? FALSE : TRUE;
    }
}