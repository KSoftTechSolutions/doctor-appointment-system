<footer class="footer footer-static footer-light navbar-border" style="position: fixed; bottom: 0; right: 0; left:0">
    <p class="clearfix text-muted text-sm-center mb-0 px-2">
        <span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; <?php echo date('Y'); ?>
            <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT
            </a>, All rights reserved.
        </span>
        <span class="float-md-right d-xs-block d-md-inline-block">Developed & Maintained by HandCraft</span>
    </p>
</footer>