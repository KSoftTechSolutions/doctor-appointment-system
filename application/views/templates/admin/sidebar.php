<!-- main menu-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <!-- main menu header-->
    <div class="main-menu-header">
        <h5 class="text-center"><?php echo ! empty($user_name) ? $user_name : 'Admin'; ?></h5>
    </div>
    <!-- / main menu header-->
    <!-- main menu content-->
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">

            <li class=" navigation-header">
                <i data-toggle="tooltip" data-placement="right" data-original-title="MAIN"
                   class="icon-ellipsis icon-ellipsis"></i>
            </li>

            <li class="<?php echo ($this->uri->segment(2) == 'dashboard') ? 'active current' : ''; ?> nav-item">
                <a href="<?php echo base_url('admin/dashboard'); ?>"><i class="icon-home3"></i>
                    <span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title"> Dashboard</span>
                </a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'department') ? 'active open' : ''; ?> nav-item">
                <a href="#"><i class="icon-grid2"></i><span data-i18n="nav.menu_levels.main" class="menu-title"> Department</span></a>
                <ul class="menu-content">
                    <li class="<?php echo ($this->uri->segment(2) == 'department' && $this->uri->segment(3) == 'add') ? 'current' : ''; ?>">
                        <a href="<?php echo base_url('admin/department/add'); ?>" data-i18n="nav.menu_levels.second_level" class="menu-item">
                            <i class="icon-plus"></i> Add Department
                        </a>
                    </li>
                    <li class="<?php echo ($this->uri->segment(2) == 'department' && $this->uri->segment(3) == 'list') ? 'current' : ''; ?>">
                        <a href="<?php echo base_url('admin/department/list'); ?>" data-i18n="nav.menu_levels.second_level" class="menu-item">
                            <i class="fa fa-list"></i> List Department
                        </a>
                    </li>
                </ul>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'doctor') ? 'active open' : ''; ?> nav-item">
                <a href="#"><i class="fa fa-user-md"></i><span data-i18n="nav.menu_levels.main" class="menu-title"> Doctor</span></a>
                <ul class="menu-content">
                    <li class="<?php echo ($this->uri->segment(2) == 'doctor' && $this->uri->segment(3) == 'add') ? 'current' : ''; ?>">
                        <a href="<?php echo base_url('admin/doctor/add'); ?>" data-i18n="nav.menu_levels.second_level" class="menu-item">
                            <i class="icon-plus"></i> Add Doctor
                        </a>
                    </li>
                    <li class="<?php echo ($this->uri->segment(2) == 'session' && $this->uri->segment(3) == 'list') ? 'current' : ''; ?>">
                        <a href="<?php echo base_url('admin/doctor/list'); ?>" data-i18n="nav.menu_levels.second_level" class="menu-item">
                            <i class="fa fa-list"></i> List Doctor
                        </a>
                    </li>
                </ul>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'patient') ? 'active current' : ''; ?> nav-item">
                <a href="<?php echo base_url('admin/patient/list'); ?>"><i class="icon-bed"></i>
                    <span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title"> Patient</span>
                </a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'appointment') ? 'active current' : ''; ?> nav-item">
                <a href="<?php echo base_url('admin/appointment/list'); ?>"><i class="icon-calendar-check-o"></i>
                    <span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title"> Appointment</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- / main menu-->