<?php $patient_session = get_current_session(); ?>
<!-- header -->
<div class="header" id="home">
    <div class="top_menu_w3layouts">
        <div class="container">
            <div class="header_left">
                <ul>
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> 1143 New York, USA</li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i> +(010) 221 918 811</li>
                    <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@example.com">info@example.com</a></li>
                </ul>
            </div>
            <div class="header_right">
                <ul class="forms_right">
                    <?php
                    if ( ! empty($patient_session))
                    {
                        echo '<li>Welcome, ' . $patient_session['name'] . '</li>';
                    }
                    else
                    {
                        echo '<li><a href="' . base_url('patient/login') . '">Log In</a></li>';
                        echo '<li><a href="' . base_url('patient/registration') . '">Sign Up</a></li>';
                    }
                    ?>
                </ul>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="content white">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url('home') ?>">
                        <h1><span class="fa fa-stethoscope" aria-hidden="true"></span>New Clinic </h1>
                    </a>
                </div>
                <!--/.navbar-header-->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav>
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo base_url('home') ?>" class="active">Home</a></li>
                            <li><a href="<?php echo base_url('about-us') ?>">About</a></li>
                            <li><a href="<?php echo base_url('department') ?>">Departments</a></li>
                            <li><a href="<?php echo base_url('appointment') ?>">Appointment</a></li>
                            <li><a href="<?php echo base_url('contact-us') ?>">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
                <!--/.navbar-collapse-->
                <!--/.navbar-->
            </div>
        </nav>
    </div>
</div>
<!-- banner -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
        <li data-target="#myCarousel" data-slide-to="2" class=""></li>
        <li data-target="#myCarousel" data-slide-to="3" class=""></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption">
                    <h3>With a Touch of <span>Kindness.</span></h3>
                    <p>Child Care Treatments</p>
                    <h6>Our Medical Center is the preferred choice for diplomats and employees from 64 embassies, consulates and UN agencies, as well as private patients from
                        over 60 countries.</h6>
                </div>
            </div>
        </div>
        <div class="item item2">
            <div class="container">
                <div class="carousel-caption">
                    <h3>With a Touch of <span>Kindness.</span></h3>
                    <p>Child Care Treatments</p>
                    <h6>Our Medical Center is the preferred choice for diplomats and employees from 64 embassies, consulates and UN agencies, as well as private patients from
                        over 60 countries.</h6>
                </div>
            </div>
        </div>
        <div class="item item3">
            <div class="container">
                <div class="carousel-caption">
                    <h3>With a Touch of <span>Kindness.</span></h3>
                    <p>Child Care Treatments</p>
                    <h6>Our Medical Center is the preferred choice for diplomats and employees from 64 embassies, consulates and UN agencies, as well as private patients from
                        over 60 countries.</h6>
                </div>
            </div>
        </div>
        <div class="item item4">
            <div class="container">
                <div class="carousel-caption">
                    <h3>With a Touch of <span>Kindness.</span></h3>
                    <p>Child Care Treatments</p>
                    <h6>Our Medical Center is the preferred choice for diplomats and employees from 64 embassies, consulates and UN agencies, as well as private patients from
                        over 60 countries.</h6>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="fa fa-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="fa fa-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!-- The Modal -->
</div>
<!--//banner -->