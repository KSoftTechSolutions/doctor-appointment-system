<?php $patient_session = get_current_session(); ?>
<!-- header -->
<div class="header" id="home">
    <div class="top_menu_w3layouts">
        <div class="container">
            <div class="header_left">
                <ul>
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> 1143 New York, USA</li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i> +(010) 221 918 811</li>
                    <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@example.com">info@example.com</a></li>
                </ul>
            </div>
            <div class="header_right">
                <ul class="forms_right">
                    <?php
                    if ( ! empty($patient_session))
                    {
                        echo '<li style="color: white">Welcome, ' . $patient_session['name'] . '</li>';
                        echo '<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>';
                        echo '<li><a href="' . base_url('patient/logout') . '">Logout</a></li>';
                    }
                    else
                    {
                        echo '<li><a href="' . base_url('patient/login') . '">Log In</a></li>';
                        echo '<li><a href="' . base_url('patient/registration') . '">Sign Up</a></li>';
                    }
                    ?>
                </ul>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="content white">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url('home'); ?>">
                        <h1><span class="fa fa-stethoscope" aria-hidden="true"></span>New Clinic </h1>
                    </a>
                </div>
                <!--/.navbar-header-->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav>
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo base_url('home') ?>">Home</a></li>
                            <li><a href="<?php echo base_url('about-us') ?>" class="<?php echo($this->uri->segment(1) == 'about-us' ? 'active' : ''); ?>">About</a></li>
                            <li><a href="<?php echo base_url('department') ?>" class="<?php echo($this->uri->segment(1) == 'department' ? 'active' : ''); ?>">Departments</a>
                            </li>
                            <li><a href="<?php echo base_url('appointment') ?>" class="<?php echo($this->uri->segment(1) == 'appointment' ? 'active' : ''); ?>">Appointment</a>
                            </li>
                            <li><a href="<?php echo base_url('contact-us') ?>" class="<?php echo($this->uri->segment(1) == 'contact-us' ? 'active' : ''); ?>">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!--/.navbar-collapse-->
                <!--/.navbar-->
            </div>
        </nav>
    </div>
</div>
<!-- banner -->
<div class="banner_inner_content_agile_w3l">

</div>
<!--//banner -->