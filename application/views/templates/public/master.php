<!DOCTYPE html>
<html lang="zxx">
<head>
    <title><?php echo ! empty($page_title) ? $page_title. ' | New Clinic' : 'New Clinic'; ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="New Clinic"/>

    <script type="application/x-javascript">

        var base_url = '<?php echo base_url(); ?>';

        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

    <?php echo $template_css; ?>

    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
</head>
<body>

<?php echo $template_header; ?>

<?php echo $template_content; ?>

<?php echo $template_footer; ?>

<?php echo $template_js; ?>

<script>
    $('ul.dropdown-menu li').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
</script>

</body>
</html>