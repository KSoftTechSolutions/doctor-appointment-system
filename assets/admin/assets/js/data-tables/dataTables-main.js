$(document).ready(function () {

    var columns = [];
    var rowsGroup = null;
    var export_title = '';

    if (export_file_for == 'doctor')
    {
        columns = [1, 2, 3, 4, 5, 6];
        export_title = 'Doctor Details';
    }
    else if (export_file_for == 'department')
    {
        columns = [1, 2, 3];
        export_title = 'Department Details';
    }
    else if (export_file_for == 'patient')
    {
        columns = [1, 2, 3, 4, 5, 6, 7, 8];
        export_title = 'Patient Details';
    }

    else if (export_file_for == 'appointment')
    {
        columns = [1, 2, 3, 4, 5, 6];
        export_title = 'Appointment Details';
    }

    initTables(export_title, columns, rowsGroup);
});

function initTables(export_title, columns, rowsGroup)
{
    $("table.dyntable:visible").each(function (i, ele) {
        var ele = $(ele);

        var source = ele.attr('source');

        var jsonStr = ele.attr('jsonInfo');

        var max_rows = ele.attr("max_rows");

        ele.DataTable({
            bDestroy: true,
            bFilter: true,
            bLengthChange: true,
            iDisplayLength: 10,
            bSort: true,
            bServerSide: true,
            bProcessing: true,
            bJQueryUI: false,
            sPaginationType: "full_numbers",
            sAjaxSource: source,
            oLanguage: {
                sEmptyTable: 'No record found'
            },
            aoColumns: eval(jsonStr),
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            order: [[1, 'asc']],
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'print',
                    text: 'Print All',
                    title: export_title,
                    exportOptions: {
                        modifier: {
                            selected: null
                        },
                        columns: columns
                    }
                },
                {
                    extend: 'print',
                    text: 'Print Selected',
                    title: export_title,
                    exportOptions: {
                        modifier: {
                            selected: true
                        },
                        columns: columns
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: 'Excel All',
                    title: export_title,
                    exportOptions: {
                        modifier: {
                            selected: null
                        },
                        columns: columns
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: 'Excel Selected',
                    title: export_title,
                    exportOptions: {
                        modifier: {
                            selected: true
                        },
                        columns: columns
                    }
                }
                // {
                //     extend: 'csvHtml5',
                //     text: 'CSV All',
                //     title: export_title,
                //     exportOptions: {
                //         modifier: {
                //             selected: null
                //         },
                //         columns: columns
                //     }
                // },
                // {
                //     extend: 'csvHtml5',
                //     text: 'CSV Selected',
                //     title: export_title,
                //     exportOptions: {
                //         modifier: {
                //             selected: true
                //         },
                //         columns: columns
                //     }
                // },
                // {
                //     extend: 'pdfHtml5',
                //     text: 'PDF',
                //     title: export_title,
                //     exportOptions: {
                //         modifier: {
                //             selected: null
                //         },
                //         columns: columns
                //     }
                // }
            ]
        });
    });
}
