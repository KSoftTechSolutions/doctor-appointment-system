$(document).ready(function () {

    $('#mobile_number, #zipcode, #height, #weight').numeric();

    $("#date_of_birth").datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        toggleActive: true,
        endDate: '-3d'
    });

    $("#appointment_date").datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        toggleActive: true,
        startDate: '-3d'
    });

    $('#dept_id').on("change", function () {

        var dept_id = $(this).val();

        $.ajax({
            url: base_url + 'appointment/get-doctor-by-dept-ajax',
            data: {
                dept_id: dept_id
            },
            method: 'POST',
            dataType: 'json'
        }).done(function (response) {

            var toAppend = '';

            $('#doctor_id').empty();

            if (response.status == 'true')
            {
                toAppend += '<option value="">' + 'Select Doctor' + '</option>';

                $.each(response.data, function (k, v) {
                    toAppend += '<option value=' + v.id + '>' + v.name + '</option>';
                });

                $('#doctor_id').append(toAppend);
            }
            else
            {
                toAppend = '<option value="">' + 'No Doctor Found' + '</option>';

                $('#doctor_id').append(toAppend);
            }
        });
    });

    $("#save_form").validate({
        ignore: [],
        errorContainer: $('#errorContainer'),
        errorLabelContainer: $('#errorContainer ul'),
        wrapper: 'li',
        onfocusout: false,
        highlight: function (element, errorClass) {
            if ($(element).hasClass('select-2'))
            {
                $(element).next('.select2-container').addClass(errorClass);
            }
            else
            {
                $(element).addClass(errorClass);
            }
        },
        unhighlight: function (element, errorClass) {
            if ($(element).hasClass('select-2'))
            {
                $(element).next('.select2-container').removeClass(errorClass);
            }
            else
            {
                $(element).removeClass(errorClass);
            }
        }
    });
});