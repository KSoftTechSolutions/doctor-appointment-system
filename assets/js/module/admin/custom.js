$(document).ready(function () {

    $('#mobile').numeric();

    $("#save_form").validate({
        ignore: [],
        errorContainer: $('#errorContainer'),
        errorLabelContainer: $('#errorContainer ul'),
        wrapper: 'li',
        onfocusout: false,
        highlight: function (element, errorClass) {
            if ($(element).hasClass('select-2'))
            {
                $(element).next('.select2-container').addClass(errorClass);
            }
            else
            {
                $(element).addClass(errorClass);
            }
        },
        unhighlight: function (element, errorClass) {
            if ($(element).hasClass('select-2'))
            {
                $(element).next('.select2-container').removeClass(errorClass);
            }
            else
            {
                $(element).removeClass(errorClass);
            }
        }
    });

});